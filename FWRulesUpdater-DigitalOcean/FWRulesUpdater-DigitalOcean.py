#!python

import requests
import ConfigParser
import os
import sys


os.chdir(os.path.dirname(sys.argv[0]))    # We change working dir to script location

config = ConfigParser.RawConfigParser()
config.optionxform = str                  # For case sensitive config file
config.read('./.config')

lastip = config.get("main", "lastip")

# Gesting public IP address

public_ip = requests.get('https://api.ipify.org').text


if public_ip != lastip:

    DOAPIKey = config.get("main", "DOAPIKey")
    DOFWGroupID = config.get("main", "DOFWGroupID")

    r = requests.get('https://api.digitalocean.com/v2/firewalls/' + DOFWGroupID, headers={'Authorization': 'Bearer ' + DOAPIKey})
    inbound_rules = r.json()['firewall']['inbound_rules']
    for rule in inbound_rules:
        addresses = rule['sources']['addresses']
        if lastip in addresses:
            # We delete the rule...
            data = {'inbound_rules': [rule]}
            r = requests.delete('https://api.digitalocean.com/v2/firewalls/' + DOFWGroupID + '/rules', headers={'Authorization': 'Bearer ' + DOAPIKey} , json=data)

            # And create a new one:
            protocol = rule['protocol']
            ports = rule['ports']
            addresses.remove(lastip)
            addresses.append(public_ip)
            data = {"inbound_rules": [{"protocol": protocol, "ports": ports, "sources": {"addresses": addresses}}]}
            r = requests.post('https://api.digitalocean.com/v2/firewalls/' + DOFWGroupID + '/rules', headers={'Authorization': 'Bearer ' + DOAPIKey}, json=data)

    # Replace lastip value in conf
    config.set('main', 'lastip', public_ip)
    with open('.config', 'w') as configfile:
        config.write(configfile)
