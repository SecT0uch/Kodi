# LibreElec

The repo contains the list of my scripts running on LibreElec.

## bt-autoconnect.py

This script is binded to a key of the remote controller via **~/.kodi/userdata/keymaps/gen.xml**.

* Connection to the bluetooth headset.
* Switch the audio profile TV/Bluetooth, with the help of the Audio Profiles add-on.

## mvAnimes.sh

This script is runned each startup of Kodi and written with the help of the FreeboxOS API.
You need first an [**app_token**](https://framagit.org/SecT0uch/Tools/tree/master/FreeboxTokenGen).

Called by **~.kodi/addons/service.mv.animes/addon.xml**, via **default.py**.

* Removal of finished/seeding torrents.
* Move the files in appropriates folders, for Kodi to scan properly.

## DNSUpdater-Porkbun

This script is supposed to be run with cron.
For dynamic public IP, it changes the DNS record every X minutes on ~~Pornhub~~ Porkbun.

You need to install the requests python module with `sudo -H pip install requests -U`
You need to create the file `.config` and place it in the same directory as the script.
Here is the template :
```
[main]
user=username
pass=$up3RL0nGP@$$worD
fqdn=host.example.com
lastip = X.X.X.X
```

## FWRulesUpdater-Vultr

This script is supposed to be run with cron.
For dynamic public IP, it changes the Vultr firewall rules every X minutes to allow new public ip.

You need to install the requests python module with `sudo -H pip install requests -U`
You need to create the file `.config` and place it in the same directory as the script.
Here is the template :
```
[main]
vultrAPIKey=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
vultrFWGroupID=deadbeef
lastip=X.X.X.X
comment=comment
```

## FWRulesUpdater-DigitalOcean

This script is supposed to be run with cron.
For dynamic public IP, it changes the DigitalOcean firewall rules every X minutes to allow new public ip.

You need to install the requests python module with `sudo -H pip install requests -U`
You need to create the file `.config` and place it in the same directory as the script.
Here is the template :
```
[main]
DOAPIKey=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
DOFWGroupID=deadbeef
lastip=X.X.X.X
```

# From Android

Install Termux and Termux:Widget.
Place the script under `~/.shortcuts/tasks/`
Add the Widget on menu and run manually every time you need to update the rules.
# From Android

Install Termux and Termux:Widget.
Place the script under `~/.shortcuts/tasks/`
Add the Widget on menu and run manually every time you need to update the rules.
