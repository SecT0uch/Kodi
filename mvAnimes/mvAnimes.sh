#!/bin/bash

app_token=`cat ./app_token`
status='auth_required'

while [ "$status" ==  "auth_required" ]; do
  result=`curl -H "Content-type: application/json" -X GET http://mafreebox.freebox.fr/api/v4/login/ 2> /dev/null`
  challenge=`echo -n "$result" | cut -f 10 -d '"'`
  password=`echo -n "$challenge" | openssl sha1 -hmac "$app_token"`
  password=`echo -n "$password" | cut -f 2 -d " "`
  login=`curl -X POST http://mafreebox.freebox.fr/api/v4/login/session/ -d '{"app_id":"Kodi.app", "password":"'$password'"}' 2>/dev/null`
  session_token=`echo -n "$login" | cut -f 6 -d '"'`

  while [ "$session_token" ==  "success" ]; do
    result=`curl -H "Content-type: application/json" -X GET http://mafreebox.freebox.fr/api/v4/login/ 2> /dev/null`
    challenge=`echo -n "$result" | cut -f 10 -d '"'`
    password=`echo -n "$challenge" | openssl sha1 -hmac "$app_token"`
    password=`echo -n "$password" | cut -f 2 -d " "`
    login=`curl -X POST http://mafreebox.freebox.fr/api/v4/login/session/ -d '{"app_id":"Kodi.app", "password":"'$password'"}' 2>/dev/null`
    session_token=`echo -n "$login" | cut -f 6 -d '"'`
  done

  list=`curl -H "X-Fbx-App-Auth: $session_token" -X GET http://mafreebox.freebox.fr/api/v4/downloads/ 2> /dev/null`
  status=`echo $list | cut -f 24 -d '"'`
done
list=`echo $list | sed 's/^.*\[{/{/' | sed 's/\]}//' | sed 's/},{/}\n{/g'`
IFS=$'\n'
for torrent in `echo "$list"`; do
  name=`echo $torrent | cut -f 46 -d '"'`
  id=`echo $torrent | cut -f 33 -d '"' | sed 's/:\|,//g'`
  status=`echo $torrent | cut -f 18 -d '"'`
  if [ "$status" ==  'done' ] || [ "$status" == 'seeding' ]; then
    case "$name" in
      *Boruto*)
        curl -H "X-Fbx-App-Auth: $session_token" -X DELETE http://mafreebox.freebox.fr/api/v4/downloads/$id 2> /dev/null

        src=`python -c "import base64;str='/Elements/VIDEO/Mangas/$name';print(base64.b64encode(str.encode()).decode())"`
        dest=`python -c "import base64;str='/Elements/VIDEO/Mangas/Boruto';print(base64.b64encode(str.encode()).decode())"`
        curl -H "Content-type: application/json" -H "X-Fbx-App-Auth: $session_token" -X POST http://mafreebox.freebox.fr/api/v4/fs/mv/ -d '
        {
           "files": ["'$src'"],
           "dst": "'$dest'",
           "mode": "overwrite"
        }    ' 2>/dev/null
        ;;
    esac
    case "$name" in
      *One_Piece*)
        curl -H "X-Fbx-App-Auth: $session_token" -X DELETE http://mafreebox.freebox.fr/api/v4/downloads/$id 2> /dev/null

        src=`python -c "import base64;str='/Elements/VIDEO/Mangas/$name';print(base64.b64encode(str.encode()).decode())"`
        dest=`python -c "import base64;str='/Elements/VIDEO/Mangas/One Piece';print(base64.b64encode(str.encode()).decode())"`
        curl -H "Content-type: application/json" -H "X-Fbx-App-Auth: $session_token" -X POST http://mafreebox.freebox.fr/api/v4/fs/mv/ -d '
        {
           "files": ["'$src'"],
           "dst": "'$dest'",
           "mode": "overwrite"
        }    ' 2>/dev/null
        ;;
    esac
  fi
done

#curl -X POST http://mafreebox.freebox.fr/api/v4/login/logout/ 2>/dev/null
